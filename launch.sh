#!/usr/bin/env bash
BOOTLOADER="aarch64-u-boot.bin"
IMAGE="aarch64-nixos.qcow2"

MONITOR="-monitor stdio"
#MONITOR="-nographic"

sudo qemu-system-aarch64 -machine virt -cpu cortex-a72 -bios $BOOTLOADER \
	-drive if=none,file=$IMAGE,id=mydisk \
	-device ich9-ahci,id=ahci \
	-device ide-drive,drive=mydisk,bus=ahci.0 \
	-netdev bridge,br=virbr0,id=net0 \
	-device virtio-net-pci,netdev=net0 \
	$MONITOR -smp 6 -m 3G
