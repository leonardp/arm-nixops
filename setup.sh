#!/usr/bin/env bash

# download and create image
wget https://hydra.nixos.org/build/128467347/download/1/nixos-sd-image-20.03.3126.53c37f9d804-aarch64-linux.img.bz2
bzip2 -d nixos-sd-image-20.03.3126.53c37f9d804-aarch64-linux.img.bz2
qemu-img convert -f raw -O qcow2 nixos-sd-image-20.03.3126.53c37f9d804-aarch64-linux.img aarch64-nixos.qcow2
qemu-img resize aarch64-nixos.qcow2 +18G

# build u-boot
nix build  -f channel:nixos-20.03 pkgsCross.aarch64-multiplatform.ubootQemuAarch64
