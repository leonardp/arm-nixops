{ config, pkgs, lib, ... }:
{
  networking.hostName = "freshmeat";

  # !mui importante!
  hardware.enableRedistributableFirmware = true;

  # NixOS wants to enable GRUB by default
  boot.loader.grub.enable = false;
  # Enables the generation of /boot/extlinux/extlinux.conf
  boot.loader.generic-extlinux-compatible.enable = true;
 
  boot.kernelPackages = pkgs.linuxPackages_latest;
  
  fileSystems = {
    "/" = {
      device = "/dev/disk/by-label/NIXOS_SD";
      fsType = "ext4";
    };
  };

  i18n = {
    consoleFont = "Lat2-Terminus16";
    consoleKeyMap = "de";
    defaultLocale = "en_US.UTF-8";
  };

  environment.variables = {
    EDITOR = "vim";
  };

  services.openssh = {
    enable = true;
    passwordAuthentication = false;
    challengeResponseAuthentication = false;
  };

  users.extraUsers.root.openssh.authorizedKeys.keys = [ 
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPc5dear8nQf3y1iUzTHibOlJDtdQjcxQXF28d8hyssF leo@private"
  ];

  networking.wireless = {
    enable = true;
    userControlled.enable = true;
    networks = {
      "my_wifi" = {
        psk = "my_password";
      };
    };
  };

  environment.systemPackages = with pkgs; [
    htop
    vim
  ];
}
